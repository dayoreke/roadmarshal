__author__ = 'oreke'

class InvalidUserException(Exception):
    pass


class InvalidPasskeyException(Exception):
    pass


class NotFoundException(Exception):
    pass


class DatastoreException(Exception):
    pass


class AccessDeniedException(Exception):
    pass

class NullPointerException(Exception):
    pass

class SaveScoreException(Exception):
    pass

class PropertyException(Exception):
    pass

