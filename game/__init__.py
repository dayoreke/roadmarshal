__author__ = 'oreke'

import os
import sys

sys.path.insert(0, os.path.join(os.path.abspath('.'), 'lib/libs.zip'))

from flask import Flask
import config
from db.model import *

SECRET_KEY = "app.devfest.moments"

app = Flask("game")
app.config.from_object(config.ProductionConfig)

from view import megatron

app.register_blueprint(megatron.blueprint)
