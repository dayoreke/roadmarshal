__author__ = 'oreke'

class Config(object):
    DEBUG = True
    #CACHE_TYPE = "gaememcached"
    SECRET_KEY = "app.devfest.moments"


class DebugConfig(Config):
    pass

class ProductionConfig(Config):
    DEBUG = False
    # change cache type too

