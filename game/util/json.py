__author__ = 'oreke'

from flask import json

def dump(obj):
    return json.dumps(obj, separators=(",", ":"))

def load(string):
    return json.loads(string)
