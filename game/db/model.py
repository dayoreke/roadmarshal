__author__ = 'oreke'

from google.appengine.ext import db

class UserAccount(db.Model):
    emailAddy = db.EmailProperty()
    fullName = db.StringProperty()
    nickName = db.StringProperty()
    signUpDate = db.DateTimeProperty()
    dateInfo = db.DateTimeProperty(auto_now=True)
    topScoreClassic = db.IntegerProperty(default=0)
    topScoreArcade = db.IntegerProperty(default=0)
    classicList = db.ListProperty(int)
    highestLevel = db.IntegerProperty(default=1)
    profilePicture = db.StringProperty()

class ShareScore(db.Model):
    classic = db.BooleanProperty()
    score = db.IntegerProperty()
    userId = db.StringProperty()
