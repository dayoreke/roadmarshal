__author__ = 'oreke'

from flask import Blueprint, render_template, jsonify, abort, make_response, json
from flask import request
from flask import session
from flask import g
from flask import current_app as master

import manager
from game.exception import *


app = Blueprint("megatron", "megatron")

@app.before_request
def prefetchUser():
    g.user = None
    if "userId" in session:
        g.user = manager.getUserFromId(session["userId"])


@app.route("/")
def home():
    iframe = ["<iframe id='iframe' allowfullscreen='true' src='{}'",
    "width='852px' height='480px' frameborder='0' scrolling='no'",
    "marginheight='0' marginwidth='0'></iframe>"]
    return render_template("index.html", IFRAME = " ".join(iframe))

@app.route("/login", methods=["POST"])
def login():
    try:
        accessToken = request.form.get("access_token")
        data = json.loads(request.form.get("json"))
        g.user = manager.registerUser(data)

        #temp
        session['userId'] = data["id"]
        session['accessToken'] = accessToken
        return "successfully logged in to the game!"

    except Exception as e:
        master.logger.error("Login/SignUp failed: {}".format(e.message))
        return make_response(jsonify(error=e.message), 501)

@app.route("/logout", methods=["POST", "GET"])
def logout():
    accessToken = "access_token"
    if g.user:
        session.pop("userId")
        accessToken = session.pop("accessToken")
    return  accessToken

@app.route("/classicshare", methods=["GET"])
def classicScoreUrl():
    if not g.user:
        return abort(401)
    try:
        classicId = manager.generateClassicId()
        return str(classicId)
    except Exception as e:
        return jsonify(error=e.message)


@app.route("/myscore/<int:scoreid>", methods=["GET"])
def sendshareurl(scoreid):
    if not scoreid: abort(404)
    scoreShare = manager.getScoreById(scoreid)
    if scoreShare:
        score = scoreShare.score
        user = manager.getUserFromId(scoreShare.userId)
        gType = "classic" if scoreShare.classic else "arcade"
        params = dict(newscore = score, nickname=user.nickName, gametype=gType, id=scoreid)
        return render_template("sharescore.html", DATA = params)
    else:
        abort(404)

@app.route("/savearcade", methods=["POST"])
def savearcade():
# i am kind of confused here o, dunno if i will use the GET method first to store d score b4 saving(posting) to the datastore, So, i just posted directly
    score = request.values.get("score", type=int)
    if g.user:
        try:
            #post the score from ajax to datastore....*huh?*
            scoreid = manager.saveScoreArcade(score)
            return str(scoreid)
        except SaveScoreException:
            return jsonify(status = "error of life")
    else:
        return abort(401)

            
            
@app.route("/toparcade",methods = ["GET"])
def toparcade():
    if g.user:
        score = g.user.topScoreArcade
        return str(score)
    else:
        return abort(401)



@app.route("/arcadeboard", methods = ["GET"])
# get the leader board
def arcadeboard():
    topScorers = manager.getLeaderBoard(False)
    nicknameList = []
    topscoreList = []
    pictureList = []
    for value in topScorers:
        nicknameList.append(value.fullName or value.nickName)
        topscoreList.append(str(value.topScoreArcade))
        pictureList.append(value.profilePicture)
    data = dict(nameList = "|".join(nicknameList),
                scoreList = "|".join(topscoreList), pictureList="|".join(pictureList))
    response = make_response(jsonify( c2dictionary=True, data=data))
    #response.headers["Access-Control-Allow-Origin"] = "*"
    return response



@app.route("/topclassic",methods = ["GET"])
def topclassic():
    if g.user:
        score = g.user.topScoreClassic
        return str(score)
    else:
        return abort(401)


@app.route("/classicboard", methods = ["GET"])
# get the leader board
def classicboard():
    topScorers = manager.getLeaderBoard(True)
    nicknameList = []
    topscoreList = []
    pictureList = []
    for value in topScorers:
        nicknameList.append(value.fullName or value.nickName)
        topscoreList.append(str(value.topScoreClassic))
        pictureList.append(value.profilePicture)
    data = dict(nameList = "|".join(nicknameList),
                scoreList = "|".join(topscoreList), pictureList="|".join(pictureList))
    response = make_response(jsonify( c2dictionary=True, data=data))
    #response.headers["Access-Control-Allow-Origin"] = "*"
    return response

@app.route("/saveclassic",methods = ["POST"])
def saveclassic():
    score = request.values.get("score", type=int)
    level = request.values.get("level", type=int)
    nextLevel = request.values.get("nextLevel", type=int)
    if g.user:
        try:
            manager.saveScoreClassic(score, level, nextLevel)
            return ""
        except SaveScoreException:
            return jsonify(score = score, level = level, status = "error of life")
    else:
        return abort(401)

@app.route("/_super/classic", methods=["POST"])
def saveClassicWrapped():
    userId = request.form.get("userId", "")
    level = request.form.get("level", type=int)
    score = request.form.get("score", type=int)
    try:
        manager.saveClassicWrapper(userId,level, score)
        return "success"
    except Exception as e:
        return "failure. {}".format(e.message)

@app.route("/highestlevel",methods = ["GET"])
def highestlevel():
    if g.user:
        level = g.user.highestLevel
        return str(level)
    else:
        abort(401)

@app.route("/levelscore/<int:level>",methods = ["GET"])
def levelscore(level):
    if g.user:
        score = g.user.classicList[level-1]
        return str(score)
    else:
        abort(401)

@app.route("/feedback")
def feedback():
    return render_template("feedback.html")


@app.app_errorhandler(500)
def serverError(error):
    return render_template("500.html")

@app.app_errorhandler(404)
def notFound(error):
    return render_template("404.html")
