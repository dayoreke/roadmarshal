import datetime

__author__ = 'oreke'

from flask import g
from flask import current_app as master

from game.db.model import *
from game.exception import *


def registerUser(googleuser):
    gameuser = UserAccount.get_by_key_name(googleuser["id"])
    if not gameuser:
        gameuser = UserAccount(key_name=googleuser["id"])
        gameuser.emailAddy = googleuser.get("email", "")
        gameuser.classicList = [0]*8
        gameuser.signUpDate = datetime.datetime.now()
    if not gameuser.signUpDate:
        gameuser.signUpDate = datetime.datetime.now()
    gameuser.nickName = googleuser.get("given_name", "Player")
    gameuser.fullName = googleuser.get("name", gameuser.nickName)
    gameuser.profilePicture = googleuser.get("picture", "")
    try:
        gameuser.put()
        return gameuser
    except:
        raise DatastoreException("Registration not successful. Please try again!")


def getUserFromId(userId):
    try:
        return UserAccount.get_by_key_name(userId)
    except Exception as e:
        master.logger.error(e.message)
        #flash("Error while fetching user info")
        return None


def getScoreById(scoreId):
    try:
        return ShareScore.get_by_id(scoreId)
    except:
        return None


def saveScoreArcade(scoreArcade):
    try:
        if scoreArcade > g.user.topScoreArcade:
            g.user.topScoreArcade = scoreArcade
            g.user.put()
        scoreShare = ShareScore()
        scoreShare.score = scoreArcade
        scoreShare.userId = g.user.key().name()
        scoreShare.classic = False
        scoreShare.put()
        return scoreShare.key().id()
    except:
        raise SaveScoreException("save score has issues")


def getLeaderBoard(classic):
    accounts = UserAccount.all()
    accounts = accounts.order("-topScoreClassic") if classic else accounts.order("-topScoreArcade")
    return accounts.run(limit=10)


def saveScoreClassic(score, level, nextLevel):
    try:
        if score > g.user.classicList[level-1]:
            g.user.classicList[level-1] = score
            g.user.topScoreClassic = reduce(lambda x,y: x+y, g.user.classicList)
        if nextLevel > g.user.highestLevel:
            g.user.highestLevel= nextLevel
        g.user.put()
    except:
        raise SaveScoreException("save score has issues")

def saveClassicWrapper(userId, level, score):
    user = getUserFromId(userId)
    if user:
        user.classicList[level-1] = score
        user.topScoreClassic = reduce(lambda x,y: x+y, user.classicList)
        user.put()

def generateClassicId():
    try:
        scoreShare = ShareScore()
        scoreShare.score = g.user.topScoreClassic
        scoreShare.userId = g.user.key().name()
        scoreShare.classic = True
        scoreShare.put()
        return scoreShare.key().id()
    except Exception as e:
        raise SaveScoreException("Unable to generate ID")